# Collect, Create and Manage District/Village/Town Maps using Open Source and Open Data
Workshop material for  Collect, Create and Manage District/Village/Town Maps
using Open Source and Open Data | [Africa Digital Skills Conference | November 08 - 10, 2022](https://africadigitalskillsconference.org/)

[![Creative Commons License](http://i.creativecommons.org/l/by-sa/4.0/88x31.png)](https://creativecommons.org/licenses/by-sa/4.0/deed.de)