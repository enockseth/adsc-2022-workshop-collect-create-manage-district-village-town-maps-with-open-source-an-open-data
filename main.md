# Collect, Create and Manage District/Village/Town Maps using Open Source and Open Data
[Africa Digital Skills Conference | November 08 - 10, 2022](https://africadigitalskillsconference.org/)

![Africa Digital Skills Conference Logo](img/Adsc-Logo.png)

## Enock Seth Nyamador

* Open Data and Free Sofware advocate | [OSGeo](https://osgeo.org) and [OpenStreetMap](https://osm.org) contributor
* open@enockseth.co
* https://open.enockseth.co

[![Creative Commons License](http://i.creativecommons.org/l/by-sa/4.0/88x31.png)](https://creativecommons.org/licenses/by-sa/4.0/deed.de)



## What we learn
* Why Open Data
* What is OpenStreetMap
* Why Free and Open Source Software
* Geographic Information System (GIS) basics
* What is QGIS
* Collecting and managing data
* Creating a simple map

## Open Data
Open data is data that is openly accessible, exploitable, editable and shared by anyone for any purpose, even commercially. Open data is licensed under an open license - [Wikipedia](https://en.wikipedia.org/wiki/Open_data)

![](img/FAIR_data_principles.jpg)

## OpenStreetMap
[OpenStreetMap](https://welcome.openstreetmap.org) (OSM) is a collaborative project to create a free editable geographic database of the world

![](img/map-world-from-scratch.jpg)


### COntributing to OpenStreetMap
* Account - https://osm.org 
* Editors

![](img/josm_osm_trace.png)

## Free Software
![](img/4freedoms.png)

Source: [Why FOSS](https://www.softwarefreedomday.org/about/why-foss)

## Geographic Information System
A geographic information system (GIS) is a type of database containing geographic data (that is, descriptions of phenomena for which location is relevant), combined with software tools for managing, analyzing, and visualizing those data - [Wikipedia](https://en.wikipedia.org/wiki/Geographic_information_system)
![](img/gis_concept.png)

## QGIS
![](img/QGIS_logo%2C_2017.svg.png)

[QGIS](https://qgis.org) is a free and open-source cross-platform desktop geographic information system (GIS) application that supports viewing, editing, printing, and analysis of geospatial data.

![](img/QGIS_2.2_Valmiera_showing_new_menu_design.png)
